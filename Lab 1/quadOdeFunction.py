# quadOdeFunction : Ordinary differential equation function that models
# quadrotor dynamics. For use with one of Matlab’s ODE
# solvers (e.g., ode45).
#
#
# INPUTS
#
# t ---------- Scalar time input, as required by Matlab’s ODE function
# format.
#
# X ---------- Nx-by-1 quad state, arranged as
#
# X = [rI’,vI’,RBI(1,1),RBI(2,1),...,RBI(2,3),RBI(3,3),omegaB’]’
#
# rI = 3x1 position vector in I in meters
# vI = 3x1 velocity vector wrt I and in I, in meters/sec
# RBI = 3x3 attitude matrix from I to B frame
# omegaB = 3x1 angular rate vector of body wrt I, expressed in B
# in rad/sec
#
# omegaVec --- 4x1 vector of rotor angular rates, in rad/sec. omegaVec(i)
# is the constant rotor speed setpoint for the ith rotor.
#
# distVec --- 3x1 vector of constant disturbance forces acting on the quad’s
# center of mass, expressed in Newtons in I.
#
# P ---------- Structure with the following elements:
#
# quadParams = Structure containing all relevant parameters for the
# quad, as defined in quadParamsScript.m
#
#
# OUTPUTS
#
# Xdot ------- Nx-by-1 time derivative of the input vector X
#
# +------------------------------------------------------------------------------+
# References:
#
#
# Author: Dewey Melton dtm862
# +==============================================================================+

from sfuncs import crossproductequivalent as cpe
import numpy as np


def quadOdeFunction(x, t, omegavec, distvec, quad):
    # unpack and define desired constants
    g = 9.81
    m = quad.m
    kf = quad.kF
    kn = quad.kN
    rtrloc = quad.rotor_loc
    Jq = quad.Jq
    rtrdir = quad.omegaRdir

    # unpack x
    rvec = np.reshape(x[0:3], (3, 1), order='F')
    vvec = np.reshape(x[3:6], (3, 1), order='F')
    r_bw = np.reshape(x[6:15], (3, 3), order='F')
    omegab = np.reshape(x[15:18], (3, 1), order='F')

    # # test unpacking
    # print(x)
    # print(rvec)
    # print(vvec)
    # print(r_bw)
    # print(omegab)
    # quit()

    # calculate position derivative
    rvecdot = vvec.reshape(3,)

    # calculate velocity derivative
    g = 9.81
    zi = np.array([[0, 0, 1]]).T
    fg = -m*g*zi
    totlift = (kf.T@np.square(omegavec))*r_bw.T@zi
    vvecdot = ((fg + totlift + distvec)/m).reshape(3,)

    # # check forces
    # print("zi, fg, r_bw.T@zi:")
    # print(zi)
    # print(fg)
    # print(r_bw.T@zi)
    # print("kf.T, omegavec, np.square(omegavec):")
    # print(kf.T)
    # print(omegavec)
    # print(np.square(omegavec))
    # print("totlift, vvecdot:")
    # print(totlift)
    # print(vvecdot)
    # quit()

    # calculate angular velocity derivative
    nb = np.zeros((3, 1))
    for idx in range(len(rtrloc) + 1):
        lifttorque = (kf[idx]*omegavec[idx]**2)*cpe(rtrloc[:, idx])@zi
        dragtorque = rtrdir[idx]*(kn[idx]*omegavec[idx]**2)*zi
        nb = nb + lifttorque + dragtorque

    omegabskew = cpe(omegab)
    omegabdot = np.linalg.inv(Jq)@(nb - omegabskew@Jq@omegab).reshape(3,)

    # # check torques and omegabdot
    # print("nb:")
    # print(nb)
    # print("omegabdot:")
    # print(omegabdot)
    # quit()



    # calculate r_bw derivative
    r_bwdot = np.reshape(-(omegabskew@r_bw), (9,), order='F')

    alldots = [rvecdot, vvecdot, r_bwdot, omegabdot]
    xdot = np.hstack(alldots)

    return xdot
