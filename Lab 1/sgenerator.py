import numpy as np
from sympy import Matrix, symbols, solve_linear_system

from sfuncs import crossproductequivalent as cpe
from sfuncs import *


# this function takes in a user-defined r and v as well as the n from topSimulate
# and returns a vector of rotor speeds and initial r, e, v, omegaB
def sgenerator(r, v, n, tsim, quad):
    # unpack and define desired constants
    g = 9.81
    m = quad.m
    kf = quad.kF
    kn = quad.kN
    rtrloc = quad.rotor_loc
    Jq = quad.Jq
    rtrdir = quad.omegaRdir

    # define inertial "roll angle" of quad and associated euler angle sequence
    theta = np.arctan2(-v**2, g*r)
    e = np.array([theta, 0, 0])  # angles are 1, 2, 3
    # print(e)

    # define magnitude of needed lift vector that combines gravitational and centripetal forces
    lift = np.sqrt((m*g)**2 + (m/r*v**2)**2)

    # define inertial rotation velocity vector for quad turning about zhat, then translate to body frame
    zi = np.array([[0, 0, 1]]).T
    zi_in_b = euler2dcm(e)@zi
    T = (2*np.pi*r)/v
    wb = 2*np.pi/T*zi_in_b
    # print(wb)

    # define RHS of Nb equation
    jww = cpe(wb)@Jq@wb

    # define Nb
    nb = np.zeros((3, 4))
    for idx in range(rtrloc.shape[1]):
        fi = kf[idx]*[0, 0, 1]
        arm = rtrloc[:, idx]
        ni = kn[idx]*rtrdir[idx]*np.array([[0, 0, 1]]).T
        nb[:, [idx]] = np.cross(arm, fi).reshape(3, 1) + ni

    # form multivar expression for solver with Nb and sum of ws on LHS, jww and lift on rhs
    w1, w2, w3, w4 = symbols('w1 w2 w3 w4')
    LHS = np.vstack((nb, kf.T))
    RHS = np.vstack((jww, lift))
    system = Matrix(np.hstack((LHS, RHS)))

    # solve the system
    sqrdspds = solve_linear_system(system, w1, w2, w3, w4)

    # do a little housekeeping since the SymPy internal "float" type does not support the sqrt() operation from numpy
    rtrspds = list(sqrdspds.values())
    rtrspds = [np.sqrt(float(item)) for item in rtrspds]
    # print(rtrspds)

    # create storage struct to return and generate data in it
    s = StructType
    s.state0 = StructType
    s.omegaMat = (np.array(rtrspds).reshape(4, 1)*np.ones((1, n-1))).T
    s.tVec = np.linspace(0, tsim, n)
    s.state0.r = np.array([0, -r, 0])
    s.state0.v = np.array([v, 0, 0])
    s.state0.e = e
    s.state0.omegaB = wb.flatten()

    return s
