from sfuncs.adv_euler2dcm import *
from sfuncs.adv_dcm2euler import *
import numpy as np

# unit test to make sure my adv_e2dcm and adv_dcm2e work
seqlist = ['121', '123', '131', '132', '212', '213', '231', '232', '312', '313', '321', '323']
for order in seqlist:
    for idx in range(1001):
        precision = 10
        [angles] = np.random.rand(1, 3).tolist()
        flipside = a_dcm2e(order, a_e2dcm(order, angles))
        angles = [round(item, precision) for item in angles]
        flipside = [round(item, precision) for item in flipside]
        if angles != flipside:
            print('order: ' + str(order))
            print(idx)
            print(angles)
            print(flipside)
            break
        if idx == 1000:
            print('done')
