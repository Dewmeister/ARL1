import numpy as np

class QuadParams:
    # kF(i) is the rotor thrust constant for the ith rotor, in N / (rad / s) ^ 2
    kF = 6.11*10**-8*0.104719755**-2*np.ones((4, 1))

    # kN(i) is the rotor counter - torque constant for the ith rotor, in N-m / (rad / s) ^ 2
    kN = 1.5*10**-9*0.104719755**-2*np.ones((4, 1))

    # omegaRdir(i) indicates the ith rotor spin direction: 1 for a rotor angular % rate vector aligned with the
    # body z-axis, -1 for the opposite direction
    omegaRdir = np.array([1, -1, 1, -1])

    # rotor_loc(:, i) holds the 3x1 coordinates of the ith rotor in the body frame, in meters
    rotor_loc = 0.21*np.array([[1, 1, -1, -1],
                               [-1, 1, 1, -1],
                               [0, 0, 0, 0]])

    # Mass of the quad, in kg
    m = 0.78

    # The quad's moment of inertia, expressed in the body frame, in kg-m^2
    Jq = 1*10**-9*np.array([[1756500, 14600, 125000],
                            [14600, 3572300, -9200],
                            [125000, -9200, 4713400]])

    # The circular - disk - equivalent area of the quad 's body, in m^2
    Adisk = 0.01

    # The quad's coefficient of drag, in N-s/m^3
    Cd = 0.01

    # taum(i) is the time constant of the ith rotor, in seconds.This governs how
    # quickly the rotor responds to input voltage.
    taum = 0.05*np.ones((4, 1))

    # cm(i) is the factor used to convert motor voltage to motor angular rate
    # in steady state for the ith motor, with units of rad / sec / volt
    cm = 200*np.ones((4, 1))

    # Maximum voltage that can be applied to any motor, in volts
    eamax = 12

    # r_rotor(i) is the radius of the ith rotor, in meters
    r_rotor = 0.06*np.ones((4, 1))

    # -----------------------------------------------------------------------------------------------------------------
