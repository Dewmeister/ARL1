# dcm2euler : Converts a direction cosine matrix R_BW to Euler angles phi =
# e(1), theta = e(2), and psi = e(3) (in radians) for a 3-1-2
# rotation.
#
# Let the world (W) and body (B) reference frames be initially aligned. In a
# 3-1-2 order, rotate B away from W by angles psi (yaw, about the body Z
# axis), phi (roll, about the body X axis), and theta (pitch, about the body Y
# axis). R_BW can then be used to cast a vector expressed in W coordinates as
# a vector in B coordinates: vB = R_BW * vW
#
# INPUTS
#
# R_BW ------- 3-by-3 direction cosine matrix
#
#
# OUTPUTS
#
# e ---------- 3-by-1 vector containing the Euler angles in radians: phi =
# e(1), theta = e(2), and psi = e(3). By convention, these
# should be constrained to the following ranges: -pi/2 <= phi <=
# pi/2, -pi <= theta < pi, -pi <= psi < pi.
#
# +------------------------------------------------------------------------------+
# References: Representing Attitude: Euler Angles, Unit Quaternions, and Rotation
# Vectors, Diebel, 2006
#
#
# Author: Dewey Melton dtm862
# +==============================================================================+

import numpy as np


def dcm2euler(r_bw):
    # make some assertions so that if something gets passed incorrectly, the code stops here and returns my msg
    assert isinstance(r_bw, np.ndarray), "The passed object is not a NumPy array and this function will not work"
    assert r_bw.shape == (3, 3), "The passed array is the wrong shape. It should be (3, 3)"

    # using math done outside the code, calculate phi/theta/psi based on selected elements of the passed r_bw
    phi = np.arcsin(r_bw[1, 2])
    theta = np.arctan2(-r_bw[0, 2], r_bw[2, 2])
    psi = np.arctan2(-r_bw[1, 0], r_bw[1, 1])

    # return the NumPy array of Euler angles
    return np.array([phi, theta, psi])


