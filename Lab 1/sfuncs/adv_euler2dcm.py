import numpy as np


def a_e2dcm(sequence, e):
    # make some assertions so that if something gets passed incorrectly, the code stops here and returns my msg
    assert len(e) == 3, "The given vector of Euler angles is not the right size (length = 3)."
    assert type(sequence) == str, "The passed sequence value is not the right type"
    assert len(sequence) == 3, "The passed sequence value is not the right length"

    def trig(a):
        return np.sin(a), np.cos(a)

    def r1(a):
        s, c = trig(a)
        return np.array([[1, 0, 0],
                         [0, c, s],
                         [0, -s, c]])

    def r2(a):
        s, c = trig(a)
        return np.array([[c, 0, -s],
                         [0, 1, 0],
                         [s, 0, c]])

    def r3(a):
        s, c = trig(a)
        return np.array([[c, s, 0],
                         [-s, c, 0],
                         [0, 0, 1]])

    library = {
        '1': r1,
        '2': r2,
        '3': r3
    }

    return library[sequence[2]](e[2])@library[sequence[1]](e[1])@library[sequence[0]](e[0])
