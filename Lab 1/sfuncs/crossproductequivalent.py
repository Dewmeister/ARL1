# crossProductEquivalent : Outputs the cross-product-equivalent matrix uCross
# such that for arbitrary 3-by-1 vectors u and v,
# cross(u,v) = uCross*v.
#
# INPUTS
#
# u ---------- 3-by-1 vector
#
#
# OUTPUTS
#
# uCross ----- 3-by-3 skew-symmetric cross-product equivalent matrix
#
# +------------------------------------------------------------------------------+
# References: https://en.wikipedia.org/wiki/Cross_product#Conversion_to_matrix_multiplication
#
#
# Author: Dewey Melton dtm862
# +==============================================================================+

import numpy as np



def crossproductequivalent(u):
    assert isinstance(u, np.ndarray), "The passed vector is not a NumPy array"
    u = u.flatten()
    assert u.shape == (3,), "The passed vector is not the right length (3 elements)"


    # assign each element of the passed "vector" (python list object) to a unique variable to make the next step clearer
    u1, u2, u3 = u[0], u[1], u[2]

    # create the cross-product-equivalent matrix in a NumPy array object
    ucpe = np.array([[0, -u3, u2], [u3, 0, -u1], [-u2, u1, 0]])

    # return the array object
    return ucpe


if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    testu = np.random.random((1, 3))
    testcpe = crossproductequivalent(testu)
    print(testcpe)
