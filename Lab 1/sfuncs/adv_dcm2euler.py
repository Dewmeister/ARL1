import numpy as np


def a_dcm2e(sequence, dcm):
    # make some assertions so that if something gets passed incorrectly, the code stops here and returns my msg
    assert isinstance(dcm, np.ndarray), "The passed object is not a NumPy array and this function will not work"
    assert dcm.shape == (3, 3), "The passed array is the wrong shape"
    assert type(sequence) == str, "The passed sequence value is not the right type"
    assert len(sequence) == 3, "The passed sequence value is not the right length"

    # define particular "transformation" for every euler angle sequence
    def t121(a):
        phi = np.arctan2(-a[0, 1], a[0, 2]) + np.pi
        theta = np.arccos(a[0, 0])
        psi = np.arctan2(a[1, 0], a[2, 0])
        return [phi, theta, psi]

    def t123(a):
        phi = np.arctan2(-a[2, 1], a[2, 2])
        theta = np.arcsin(a[2, 0])
        psi = np.arctan2(-a[1, 0], a[0, 0])
        return [phi, theta, psi]

    def t131(a):
        phi = np.arctan2(a[0, 2], a[0, 1])
        theta = np.arccos(a[0, 0])
        psi = np.arctan2(a[2, 0], -a[1, 0])
        return [phi, theta, psi]

    def t132(a):
        phi = np.arctan2(a[1, 2], a[1, 1])
        theta = np.arcsin(-a[1, 0])
        psi = np.arctan2(a[2, 0], a[0, 0])
        return [phi, theta, psi]

    def t212(a):
        phi = np.arctan2(a[1, 0], a[1, 2])
        theta = np.arccos(a[1, 1])
        psi = np.arctan2(a[0, 1], -a[2, 1])
        return [phi, theta, psi]

    def t213(a):
        phi = np.arctan2(a[2, 0], a[2, 2])
        theta = np.arcsin(-a[2, 1])
        psi = np.arctan2(a[0, 1], a[1, 1])
        return [phi, theta, psi]

    def t231(a):
        phi = np.arctan2(-a[0, 2], a[0, 0])
        theta = np.arcsin(a[0, 1])
        psi = np.arctan2(-a[2, 1], a[1, 1])
        return [phi, theta, psi]

    def t232(a):
        phi = np.arctan2(a[1, 2], -a[1, 0])
        theta = np.arccos(a[1, 1])
        psi = np.arctan2(a[2, 1], a[0, 1])
        return [phi, theta, psi]

    def t312(a):
        phi = np.arctan2(-a[1, 0], a[1, 1])
        theta = np.arcsin(a[1, 2])
        psi = np.arctan2(-a[0, 2], a[2, 2])
        return [phi, theta, psi]

    def t313(a):
        phi = np.arctan2(a[2, 0], -a[2, 1])
        theta = np.arccos(a[2, 2])
        psi = np.arctan2(a[0, 2], a[1, 2])
        return [phi, theta, psi]

    def t321(a):
        phi = np.arctan2(a[0, 1], a[0, 0])
        theta = np.arcsin(-a[0, 2])
        psi = np.arctan2(a[1, 2], a[2, 2])
        return [phi, theta, psi]

    def t323(a):
        phi = np.arctan2(a[2, 1], a[2, 0])
        theta = np.arccos(a[2, 2])
        psi = np.arctan2(a[1, 2], -a[0, 2])
        return [phi, theta, psi]

    library = {
        '121': t121,
        '123': t123,
        '131': t131,
        '132': t132,
        '212': t212,
        '213': t213,
        '231': t231,
        '232': t232,
        '312': t312,
        '313': t313,
        '321': t321,
        '323': t323
    }

    # using the sequence input by the user, fetch the proper function and feed it the dcm
    return library[sequence](dcm)
