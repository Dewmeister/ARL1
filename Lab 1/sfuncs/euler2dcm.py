# euler2dcm : Converts Euler angles phi = e(1), theta = e(2), and psi = e(3)
# (in radians) into a direction cosine matrix for a 3-1-2 rotation.
#
# Let the world (W) and body (B) reference frames be initially aligned. In a
# 3-1-2 order, rotate B away from W by angles psi (yaw, about the body Z
# axis), phi (roll, about the body X axis), and theta (pitch, about the body Y
# axis). R_BW can then be used to cast a vector expressed in W coordinates as
# a vector in B coordinates: vB = R_BW * vW
#
# INPUTS
#
# e ---------- 3-by-1 vector containing the Euler angles in radians: phi =
# e(1), theta = e(2), and psi = e(3)
#
#
# OUTPUTS
#
# R_BW ------- 3-by-3 direction cosine matrix
#
# +------------------------------------------------------------------------------+
# References: Representing Attitude: Euler Angles, Unit Quaternions, and Rotation
# Vectors, Diebel, 2006
#
#
# Author: Dewey Melton dtm8862
# +==============================================================================+
import numpy as np


def euler2dcm(e):
    if isinstance(e, np.ndarray):
        e = e.flatten()
        assert e.shape == (3,), "The passed vector is not the right length (3 elements)"
    elif isinstance(e, list):
        assert len(e) == 3, "The passed list is not the right length (3 elements)"

    # create cosine and sine "minivariables" to populate rotation matrices
    sph, sth, sps = np.sin(e)
    cph, cth, cps = np.cos(e)

    # create R3, R1, R2
    r1 = np.array([[1, 0, 0],
                   [0, cph, sph],
                   [0, -sph, cph]])
    r2 = np.array([[cth, 0, -sth],
                   [0, 1, 0],
                   [sth, 0, cth]])
    r3 = np.array([[cps, sps, 0],
                   [-sps, cps, 0],
                   [0, 0, 1]])

    # combine the three DCMs in to one and return it
    return r2@r1@r3
