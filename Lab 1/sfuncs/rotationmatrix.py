# rotationMatrix : Generates the rotation matrix R corresponding to a rotation
# through an angle phi about the axis defined by the unit
# vector ahat. This is a straightforward implementation of
# Euler’s formula for a rotation matrix.
#
# INPUTS
#
# ahat ------- 3-by-1 unit vector constituting the axis of rotation.
#
# phi -------- Angle of rotation, in radians.
#
#
# OUTPUTS
#
# R ---------- 3-by-3 rotation matrix
#
# +------------------------------------------------------------------------------+
# References:
#
#
# Author: Dewey Melton dtm862
# +==============================================================================+

from sfuncs import crossproductequivalent as cpe
import numpy as np


def rotationmatrix(ahat, phi):
    # generate the terms of Euler's formula
    cp = np.cos(phi)
    sp = np.sin(phi)
    at = ahat.reshape(1, 3)
    ax = cpe.crossproductequivalent(ahat)
    i = np.identity(3)

    # calculate R(ahat, phi)
    r = np.multiply(i, cp) + np.multiply(ahat*at, 1-cp) + np.multiply(ax, -sp)

    # return the rotation matrix
    return r


if __name__ == '__main__':
    np.set_printoptions(precision=3, suppress=True)
    AHAT = [0, 0, 1]
    PHI = np.pi/2
    example = [1, 0, 0]
    newR = rotationmatrix(AHAT, PHI)
    print(newR)
    print(newR@example)
