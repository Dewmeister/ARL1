# top-level script for calling simulateQuadrotorDynamics function
import numpy as np
import matplotlib.pyplot as plt

from sgenerator import sgenerator
from simulateQuadrotorDynamics import simulateQuadrotorDynamics
from sfuncs import *

# call sQD first with the provided "reference S" to show that it is similar to those provided in Ptest.mat
# integer number of time steps
N = 101

# Total simulation time, in seconds
Tsim = 2

# create S
S = StructType
S.oversampfact = 10
S.quadParams = QuadParams
S.distMat = np.zeros((N-1, 3))  # matrix of disturbance forces acting on the body, in N, expressed in I
S.tVec = np.linspace(0, Tsim, N)  # time vector, in seconds
S.omegaMat = 585*np.ones((N-1, 4))  # Rotor speeds at each time, in rad/s
S.state0 = StructType  # create state struct in S
S.state0.r = np.zeros((3,))  # in state struct, create position vector
S.state0.e = np.array([0, 0.05, 0])  # in state struct, create euler angles
S.state0.v = np.array([-0.8, 0, 0])  # in state struct, create velocity vector
S.state0.omegaB = np.array([0, 0, 0.2])  # in state struct, create angular velocity vector


P = simulateQuadrotorDynamics(S)  # call simulateQuadrotorDynamics function


# print a sampling from P.state0 to demoonstrate similarity to Ptest.mat
np.set_printoptions(precision=4)
samples = np.linspace(0, 1000, 6)
samples = [int(item) for item in samples]
for row in samples:
    print("At row: " + str(row))
    print("P.rMat:")
    print(P.rMat[row, :])
    print()
    print("P.vMat:")
    print(P.vMat[row, :])
    print()
    print("P.omegaBMat:")
    print(P.omegaBMat[row, :])
    print()
    print("P.eMat:")
    print(P.eMat[row, :])
    print()

# call simulateQuadrotorDynamics a second time with the input created by sgenerator.py
# input circle parameters
r, v = 1, 7  # m, m/s

# call sgenerator
Scirc = sgenerator(r, v, N, Tsim, S.quadParams)
Scirc.oversampfact = 10
Scirc.quadParams = QuadParams
Scirc.distMat = np.zeros((N-1, 3))  # matrix of disturbance forces acting on the body, in N, expressed in I

Pcirc = simulateQuadrotorDynamics(Scirc)

pisplits = np.linspace(0, 2*np.pi, 100)
refcircx, refcircy = np.empty(100), np.empty(100)
for idx, angle in enumerate(pisplits):
    refcircx[idx] = r*np.cos(angle)
    refcircy[idx] = r*np.sin(angle)
flatline = np.zeros_like(Scirc.tVec)

fig, ax = plt.subplots(1, 2, figsize=(6, 3))
ax[0].set_title('X, Y plane')
ax[0].set_xlabel('X (m)')
ax[0].set_ylabel('Y (m))')
ax[0].axis('equal')
ax[0].plot(Pcirc.rMat[:, 0], Pcirc.rMat[:, 1], 'b--', label="Simulated quad flight path")
ax[0].plot(refcircx, refcircy, 'k:', label='Perfect Reference Circle')
ax[0].legend()

ax[1].set_title('Z position with time')
ax[1].set_xlabel('time (s)')
ax[1].set_ylabel('Z (m))')
ax[1].axis('equal')
ax[1].plot(Scirc.tVec, flatline, 'b--', label="Initial altitude")
ax[1].plot(Scirc.tVec, Pcirc.rMat[:, 2], 'k:', label="Simulated quad flight path")
ax[1].legend()

plt.show()
