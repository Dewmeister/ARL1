# combine all functions I wrote in to one import
import numpy as np


class QuadParams:
    # kF(i) is the rotor thrust constant for the ith rotor, in N / (rad / s) ^ 2
    kF = 6.11*10**-8*0.104719755**-2*np.ones((4, 1))

    # kN(i) is the rotor counter - torque constant for the ith rotor, in N-m / (rad / s) ^ 2
    kN = 1.5*10**-9*0.104719755**-2*np.ones((4, 1))

    # omegaRdir(i) indicates the ith rotor spin direction: 1 for a rotor angular % rate vector aligned with the
    # body z-axis, -1 for the opposite direction
    omegaRdir = np.array([1, -1, 1, -1])

    # rotor_loc(:, i) holds the 3x1 coordinates of the ith rotor in the body frame, in meters
    rotor_loc = 0.21*np.array([[1, 1, -1, -1],
                               [-1, 1, 1, -1],
                               [0, 0, 0, 0]])

    # Mass of the quad, in kg
    m = 0.78

    # The quad's moment of inertia, expressed in the body frame, in kg-m^2
    Jq = 1*10**-9*np.array([[1756500, 14600, 125000],
                            [14600, 3572300, -9200],
                            [125000, -9200, 4713400]])

    # The circular - disk - equivalent area of the quad 's body, in m^2
    Adisk = 0.01

    # The quad's coefficient of drag, in N-s/m^3
    Cd = 0.01

    # taum(i) is the time constant of the ith rotor, in seconds.This governs how
    # quickly the rotor responds to input voltage.
    taum = 0.05*np.ones((4, 1))

    # cm(i) is the factor used to convert motor voltage to motor angular rate
    # in steady state for the ith motor, with units of rad / sec / volt
    cm = 200*np.ones((4, 1))

    # Maximum voltage that can be applied to any motor, in volts
    eamax = 12

    # r_rotor(i) is the radius of the ith rotor, in meters
    r_rotor = 0.06*np.ones((4, 1))

    # -----------------------------------------------------------------------------------------------------------------


class StructType:
    pass


def dcm2euler(r_bw):
    # make some assertions so that if something gets passed incorrectly, the code stops here and returns my msg
    assert isinstance(r_bw, np.ndarray), "The passed object is not a NumPy array and this function will not work"
    assert r_bw.shape == (3, 3), "The passed array is the wrong shape. It should be (3, 3)"

    # using math done outside the code, calculate phi/theta/psi based on selected elements of the passed r_bw
    phi = np.arcsin(r_bw[1, 2])
    theta = np.arctan2(-r_bw[0, 2], r_bw[2, 2])
    psi = np.arctan2(-r_bw[1, 0], r_bw[1, 1])

    # return the NumPy array of Euler angles
    return np.array([phi, theta, psi])


def euler2dcm(e):
    if isinstance(e, np.ndarray):
        e = e.flatten()
        assert e.shape == (3,), "The passed vector is not the right length (3 elements)"
    elif isinstance(e, list):
        assert len(e) == 3, "The passed list is not the right length (3 elements)"

    # create cosine and sine "minivariables" to populate rotation matrices
    sph, sth, sps = np.sin(e)
    cph, cth, cps = np.cos(e)

    # create R3, R1, R2
    r1 = np.array([[1, 0, 0],
                   [0, cph, sph],
                   [0, -sph, cph]])
    r2 = np.array([[cth, 0, -sth],
                   [0, 1, 0],
                   [sth, 0, cth]])
    r3 = np.array([[cps, sps, 0],
                   [-sps, cps, 0],
                   [0, 0, 1]])

    # combine the three DCMs in to one and return it
    return r2@r1@r3


def crossproductequivalent(u):
    assert isinstance(u, np.ndarray), "The passed vector is not a NumPy array"
    u = u.flatten()
    assert u.shape == (3,), "The passed vector is not the right length (3 elements)"

    # assign each element of the passed "vector" (python list object) to a unique variable to make the next step clearer
    u1, u2, u3 = u[0], u[1], u[2]

    # create the cross-product-equivalent matrix in a NumPy array object
    ucpe = np.array([[0, -u3, u2], [u3, 0, -u1], [-u2, u1, 0]])

    # return the array object
    return ucpe


def rotationmatrix(ahat, phi):
    # generate the terms of Euler's formula
    cp = np.cos(phi)
    sp = np.sin(phi)
    at = ahat.reshape(1, 3)
    ax = crossproductequivalent(ahat)
    i = np.identity(3)

    # calculate R(ahat, phi)
    r = np.multiply(i, cp) + np.multiply(ahat*at, 1-cp) + np.multiply(ax, -sp)

    # return the rotation matrix
    return r
