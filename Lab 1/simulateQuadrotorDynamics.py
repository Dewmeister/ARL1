# simulateQuadrotorDynamics : Simulates the dynamics of a quadrotor aircraft.
#
#
# INPUTS
#
# S ---------- Structure with the following elements:
#
# tVec = Nx1 vector of uniformly-sampled time offsets from the
# initial time, in seconds, with tVec(1) = 0.
#
# oversampFact = Oversampling factor. Let dtIn = tVec(2) - tVec(1). Then the
# output sample interval will be dtOut =
# dtIn/oversampFact. Must satisfy oversampFact >= 1.
#
#
# omegaMat = (N-1)x4 matrix of rotor speed inputs. omegaMat(k,j) is the
# constant (zero-order-hold) rotor speed setpoint for the jth rotor
# over the interval from tVec(k) to tVec(k+1).
#
# state0 = State of the quad at tVec(1) = 0, expressed as a structure
# with the following elements:
#
# r = 3x1 position in the world frame, in meters
#
# e = 3x1 vector of Euler angles, in radians, indicating the
# attitude
#
# v = 3x1 velocity with respect to the world frame and
# expressed in the world frame, in meters per second.
#
# omegaB = 3x1 angular rate vector expressed in the body frame,
# in radians per second.
#
# distMat = (N-1)x3 matrix of disturbance forces acting on the quad’s
# center of mass, expressed in Newtons in the world frame.
# distMat(k,:)’ is the constant (zero-order-hold) 3x1
# disturbance vector acting on the quad from tVec(k) to
# tVec(k+1).
#
# quadParams = Structure containing all relevant parameters for the
# quad, as defined in quadParamsScript.m
#
#
# OUTPUTS
#
# P ---------- Structure with the following elements:
#
# tVec = Mx1 vector of output sample time points, in seconds, where
# P.tVec(1) = S.tVec(1), P.tVec(M) = S.tVec(N), and M =
# (N-1)*oversampFact + 1.
#
#
# state = State of the quad at times in tVec, expressed as a structure
# with the following elements:
#
# rMat = Mx3 matrix composed such that rMat(k,:)’ is the 3x1
# position at tVec(k) in the world frame, in meters.
#
# eMat = Mx3 matrix composed such that eMat(k,:)’ is the 3x1
# vector of Euler angles at tVec(k), in radians,
# indicating the attitude.
#
# vMat = Mx3 matrix composed such that vMat(k,:)’ is the 3x1
# velocity at tVec(k) with respect to the world frame
# and expressed in the world frame, in meters per
# second.
#
# omegaBMat = Mx3 matrix composed such that omegaBMat(k,:)’ is the
# 3x1 angular rate vector expressed in the body frame in
# radians, that applies at tVec(k).
#
#
# +------------------------------------------------------------------------------+
# References:
#
#
# Author: Dewey Melton dtm862
# +==============================================================================+

import numpy as np
from scipy.integrate import odeint
from quadOdeFunction import quadOdeFunction
from sfuncs import *


def simulateQuadrotorDynamics(s):
    # unpack the input structure s
    tvec, osfact, wmat, s0, distmat, quad = s.tVec, s.oversampfact, s.omegaMat, s.state0, s.distMat, s.quadParams
    r0, e0, v0, wb0 = s0.r, s0.e, s0.v, s0.omegaB

    # create a few housekeeping vars to prepare the loop
    t0 = tvec[0]
    dtin = tvec[1] - tvec[0]

    # unpack initial state and RBI in to "X"
    x = np.empty((18,))
    x[0:3], x[3:6], x[15:18] = r0, v0, wb0
    r_bw = euler2dcm(e0)
    x[6:15] = np.reshape(r_bw, (9,), order='F')

    # set up t and x storage vectors
    thist = []
    xhist = []

    # integrate over every set of oversampling points between points in tvec
    for idx in range(len(tvec) - 1):
        # prepare arguments for qOdeFunc
        tspan = t0 + np.linspace(0, dtin, osfact + 1)
        args = (wmat[[idx], :].T, distmat[[idx], :].T, quad)

        # call odeint to do the thing
        xiter = odeint(quadOdeFunction, x, tspan, args=args)

        # set intial conditions for next iteration
        t0 = tspan[-1]
        x = xiter[-1, :]

        # put data in to storage/history lists
        if idx != len(tvec) - 2:
            thist.append(tspan[:-1])
            xhist.append(xiter[:-1, :])
        else:
            thist.append(tspan)
            xhist.append(xiter)

    tfinal = np.hstack(thist)
    xfinal = np.vstack(xhist)

    # create empty lists in output structure to hold results of ode integration
    p = StructType
    p.state = StructType
    p.state.tVec = tfinal
    p.state.rMat = xfinal[:, 0:3]
    p.state.vMat = xfinal[:, 3:6]
    p.state.omegaBMat = xfinal[:, 15:18]
    p.state.eMat = np.empty((len(tfinal), 3))
    for idx in range(xfinal.shape[0]):
        r = np.reshape(xfinal[idx, 6:15], (3, 3), order='F')
        p.state.eMat[idx, :] = dcm2euler(r)

    # # test re-packing
    # print(xfinal[0, :])
    # print(p.state.tVec[0])
    # print(p.state.rMat[0, :])
    # print(p.state.vMat[0, :])
    # print(p.state.eMat[0, :])
    # print(p.state.omegaBMat[0, :])
    # quit()
    return p
